#!/bin/sh
#
# Generate a signed bootable efi image containing both the linux kernel
# and the init ram disk
#
# Copyright (C) 2017 emily@emilyshepherd.me


# Default file locations
BOOT_DIR=/boot
CMDLINE=/etc/efi-keys/cmdline
KEYS_DIR=/etc/efi-keys
EFISTUB=/usr/lib/systemd/boot/efi/linuxx64.efi.stub
OUT=/boot/EFI/Boot/boot64.efi

# Command usage
usage() {
    echo -n "$0 [-h|--help] [-b|--boot] [-k|--kernel] [-i|--initrd] "
    echo "[-c|--cmdline] [-d|--dir] [-o|--out]"
}

part() {
    echo "--add-section .$1=$2 --change-section-vma .$1=0x$3"
}

while test -n "$1"
do
    case $1 in
        -b|--boot)
            BOOT_DIR=$2
            shift
            ;;
        -k|--kernel)
            KERNEL=$2
            shift
            ;;
        -i|--initrd)
            INITRD=$2
            shift
            ;;
        -c|--cmdline)
            CMDLINE=$2
            shift
            ;;
        -d|--dir)
            KEYS_DIR=$2
            shift
            ;;
        -o|--out)
            OUT=$2
            shift
            ;;
        -h|--help)
            usage
            exit
            ;;
        --)
            shift
            break 2
    esac

    shift
done

if test -z "$KERNEL"
then
    KERNEL=$BOOT_DIR/vmlinuz-linux
fi

if test -z "$INITRD"
then
    INITRD=$BOOT_DIR/initramfs-linux.img
fi

TEMP=$(mktemp)

objcopy \
    $(part osrel /etc/os-release 20000) \
    $(part cmdline $CMDLINE 30000) \
    $(part linux $KERNEL 40000) \
    $(part initrd $INITRD 3000000) \
    $EFISTUB $TEMP

sbsign \
    --key $KEYS_DIR/DB.key --cert $KEYS_DIR/DB.crt \
    --output $OUT \
    $TEMP

