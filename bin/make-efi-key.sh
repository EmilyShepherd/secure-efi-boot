#!/bin/sh
#
# Generate a key for use by the secure-efi-boot
#
# Copyright (C) 2017 emily@emilyshepherd.me


# Default file locations
KEYS_DIR=/etc/efi-keys

# Command usage
usage() {
    echo "$0 [-h|--help] MODE"
}

while test -n "$1"
do
    case $1 in
        -h|--help)
            usage
            exit
            ;;
        PK|KEK|DB)
            MODE=$1
            ;;
        *)
            usage
            exit 1
    esac

    shift
done

if test -z "$MODE"
then
    usage
    exit 1
fi

mkdir -p $KEYS_DIR

openssl req -new -x509 -newkey rsa:4096 -subj "/CN=$NAME $MODE/" \
    -keyout $KEYS_DIR/$MODE.key -out $KEYS_DIR/$MODE.crt \
    -days 3650 -nodes -sha256

chmod 600 $KEYS_DIR/$MODE.key $KEYS_DIR/$MODE.crt

