#!/bin/sh
#
# Install the secure-efi-boot scripts
#
# Copyright (C) 2017 emily@emilyshepherd.me

INSTALL_DIR=/usr/bin

for src in $(find bin/ -name '*.sh')
do
    dest=$INSTALL_DIR/$(basename $src .sh)

    if test "$1" == "--uninstall"
    then
        rm -f $dest
    else
        cp $src $dest
    fi
done
