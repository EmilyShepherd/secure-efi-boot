#!/bin/sh
#
# Generate a sig list and auth file for use by the secure-efi-boot
#
# Copyright (C) 2017 emily@emilyshepherd.me


# Default file locations
GUID_FILE=/etc/efi-keys/guid
KEYS_DIR=/etc/efi-keys

# Command usage
usage() {
    echo -n "$0 [-h|--help] [-g|--guid GUID] [--guid-file GUID_FILE] "
    echo "[-d|--key-dir] [-c|--cert] MODE"
}

eval set -- $(getopt \
    -o g:d:c:h \
    -l guid:,guid-file:,key-dir:,cert:,help \
    -n $0 \
    -- "$@")

while test -n "$1"
do
    case $1 in
        -g|--guid)
            GUID=$2
            shift
            ;;
        --guid-file)
            GUID_FILE=$2
            shift
            ;;
        -d|--key-dir)
            KEYS_DIR=$2
            shift
            ;;
        -c|--cert)
            CERT=$2
            shift
            ;;
        -h|--help)
            usage
            exit
            ;;
        --)
            shift
            break 2
    esac

    shift
done

case $1 in
    PK|KEK)
        AUTH=PK ;;
    DB)
        AUTH=KEK ;;
    *)
        usage
        exit 1
esac

MODE=$1

if test -z "$CERT"
then
    CERT=$KEYS_DIR/$MODE.crt
fi

if ! test -e "$CERT"
then
    echo "Missing CERT File: $CERT"
    exit 1
fi

if test -z "$GUID"
then
    if ! test -e "$GUID_FILE"
    then
        echo "Missing GUID file: $GUID_FILE"
        exit 1
    else
        GUID=$(cat $GUID_FILE)
    fi
fi

cert-to-efi-sig-list -g $GUID $CERT $MODE.esl

sign-efi-sig-list -k $KEYS_DIR/$AUTH.key -c $KEYS_DIR/$AUTH.crt \
    $MODE $MODE.esl $MODE.auth

