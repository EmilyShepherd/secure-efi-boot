#!/bin/sh
#
# Generate a GUID for use by the secure-efi-boot
#
# Copyright (C) 2017 emily@emilyshepherd.me


# Default guid file location
GUID_FILE=/etc/efi-keys/guid

# Command usage
usage() {
    echo "$0 [-h|--help] [-g|--guid GUID] [--guid-file GUID_FILE]"
}

eval set -- $(getopt -o hg: -l guid:,help,guid-file: -n $0 -- "$@")

while test -n "$1"
do
    case $1 in
        -g|--guid)
            GUID=$2
            shift
            ;;
        --guid-file)
            GUID_FILE=$2
            shift
            ;;
        -h|--help)
            usage
            exit
            ;;
        --) ;;
        *)
            usage
            exit 1
    esac

    shift
done

# If we weren't explictly given a GUID, generate one
if test -z "$GUID"
then
    GUID=$(uuidgen)
fi

mkdir -p $(dirname $GUID_FILE)

echo $GUID > $GUID_FILE
chmod 600 $GUID_FILE
