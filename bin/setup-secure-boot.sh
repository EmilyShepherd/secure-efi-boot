#!/bin/sh
#
# Setup the secure boot files
#
# Copyright (C) 2017 emily@emilyshepherd.me


make-guid

for mode in PK KEK DB
do
    make-efi-key $mode
    make-sig-list $mode
done

cp /usr/share/efitools/efi/KeyTool.efi ./
